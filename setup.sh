sudo apt-get install python-software-properties

sudo apt-get purge openjdk*
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

hdfspath="/usr/local"

# make datanode, namenode and temporary directories
sudo mkdir -p "$hdfspath/hadoop/datanode"
sudo mkdir -p "$hdfspath/hadoop/namenode"
sudo mkdir -p "$hdfspath/hadoop/tmp"

sudo tar xzf hadoop-3.0.0.tar.gz
mv hadoop-3.0.0 hadoop
sudo mv hadoop $hdfspath/hadoop
sudo chown -R hduser:hduser $hdfspath/hadoop
