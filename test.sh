hdfspath="/usr/local"

# make datanode, namenode and temporary directories
#sudo mkdir -p "$hdfspath/hadoop/datanode"
#sudo mkdir -p "$hdfspath/hadoop/namenode"
#sudo mkdir -p "$hdfspath/hadoop/tmp"

#sudo tar xzf hadoop-3.0.0.tar.gz
#mv hadoop-3.0.0 hadoop
#sudo mv hadoop $hdfspath/hadoop
#sudo chown -R hduser:hduser $hdfspath/hadoop

$configpath="$hdfspath/hadoop/hadoop/etc/hadoop"

sudo cp core-site.xml $configpath/core-site.xml
sudo cp hdfs-site.xml $configpath/hdfs-site.xml
sudo cp mapred-site.xml $configpath/mapred-site.xml
sudo cp hadoop-env.sh $configpath/hadoop-env.sh
